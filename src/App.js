import React, {useState, useEffect} from 'react';
import Classes from './App.module.scss';
import Flexi from './Flexi/Flexi';
import { updateObject } from './shared/utility';
import UserOutput from './UserOutput/UserOutput';
import { Container, Row, Form, Button } from 'reactstrap';
import axios from './axios-database';

function App() {
  const flexiConfig = {
    items: [
      {
      "name": "person_name",
      "label": "Person's Name",
      "type": "TextField",
      },
      {
      "name": "states",
      "label": "Person's state",
      "type": "DropDown",
        "values": [
          "Maharashtra",
          "Kerala",
          "Tamil Nadu"
        ]
      }
    ]
  };


  const [flex,setFlex] = useState(flexiConfig);
  const [enable,setEnable] = useState(false);
  const [userData,setUserData] = useState(null);
  

  let showData = null;
  if(userData){
    showData =<UserOutput inputData={userData}/>;
  }

  useEffect(() => {
    axios('/data.json').then(response => {
     if(response.data){
        setUserData(Object.values(response.data));
        setEnable(true);
      } 
    }).catch(error => console.log(error));
  }, [enable]);



  const formHandler = (event) => {
    event.preventDefault();
    setEnable(false);
    const formData = {};
    for (let formElementIdentifier in flex) {
      formData[formElementIdentifier] = flex[formElementIdentifier];
    }

    setFlex(formData)
    axios.post('/data.json',flex).then(response => {
      console.log(response);
      setEnable(true)
    }).catch(error => console.log(error));
  }

  const inputChangedHandler = (event, inputIdentifier) => {
              
      const updatedFormElement = updateObject(flex[inputIdentifier], {
          value: event.target.value,
      });

      const updatedOrderForm = updateObject(flex.items, {
          [inputIdentifier]: updatedFormElement
      });
      
      
      setFlex({items: updatedOrderForm});
  }

  const formElementArray = [];
  for(let key in flexiConfig.items){
    formElementArray.push({
      id: key,
      config: flexiConfig.items[key]
    })
  }

  
  return (
    <>
      <Container>
          <Form onSubmit={formHandler}>
            <Row md="2">
            {formElementArray.map(formElement => (
              <Flexi key={formElement.id}
                elementType={formElement.config.type}
                elementConfig={formElement.config.values}
                label={formElement.config.label}
                changed={(event) => inputChangedHandler(event, formElement.id)} 
              />
            ))}
            </Row>
            <Button className={Classes.submitButton} color="primary">Submit</Button>
          </Form>
      </Container>
      {showData}
    </>
  );
}

export default App;
