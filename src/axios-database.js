import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://falabella-27be9.firebaseio.com/'
});

export default instance;