import React from 'react';
import { Container, Row, Col, Table } from 'reactstrap';

const UserOutput = (props) => {

  return (
    <Container>
         <Row md="1">
             <Col>
                <Table>
                    <tbody>
                            {props.inputData.map((element,index) => (
                                <tr key={index}>
                                {element.items.map((el,index)=>(
                                    <td key={index}>
                                        {el.value}
                                    </td>
                                ))}
                                </tr>
                            ))}
                            
                    </tbody>
                </Table>
             </Col>
         </Row>
    </Container>
  );
}

export default UserOutput;
