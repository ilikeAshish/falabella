import React from 'react';
import classes from './Flexi.module.scss';
import {Input, Col, Label, FormGroup} from 'reactstrap';

const input = ( props ) => {
    let inputElement = null;
    const inputClasses = [classes.InputElement];

    if (props.invalid && props.shouldValidate && props.touched) {
        inputClasses.push(classes.Invalid);
    }

    switch ( props.elementType ) {
        case ( 'TextField' ):
            inputElement = <Input
                className={inputClasses.join(' ')}
                {...props.elementConfig}
                onChange={props.changed} required />;
            break;
        case ( 'DropDown' ):
            inputElement = (
                <Input type="select"
                    className={inputClasses.join(' ')}
                    onChange={props.changed} required>
                    <option value="">None</option>
                    {props.elementConfig.map(option => (
                        <option key={option} value={option}>
                            {option}
                        </option>
                    ))}
                </Input>
            );
            break;
        default:
            inputElement = null;
    }

    return (
        <Col>
            <FormGroup>
                <Label className={classes.Label}>{props.label}</Label>
                {inputElement}
            </FormGroup>
        </Col>
    );

};

export default input;